///-----------------------------------------------------------------
///
/// @file      IPFinderApp.h
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:07 PM
/// @section   DESCRIPTION
///            IPFinderApp class declaration
///
///------------------------------------------------------------------

#ifndef __IPFINDERFRMApp_h__
#define __IPFINDERFRMApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/cmdline.h>

class IPFinderFrmApp : public wxApp
{
	public:
		bool OnInit();
		wxCmdLineParser parser;
		wxString defaultExportPath;
		wxString defaultConfigPath;
		wxString portableAppsPath;
		int OnExit();
};

static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
	{ wxCMD_LINE_SWITCH, "h", "help", "Displays help on the command line parameters",
		wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
	{ wxCMD_LINE_OPTION, "e", "exportpath", "The default location to save exported logs",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
//	{ wxCMD_LINE_OPTION, "c", "configpath", "The location of the config file",
//		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "portableappspath", "portableappspath", "The location of the PortableApps directory",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_NONE }
};


#endif
#define IDI_APPICON 101
