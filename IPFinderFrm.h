///-----------------------------------------------------------------
///
/// @file      IPFinderFrm.h
/// @author    Ken Herbert
/// Created:   3/05/2012 12:31:08 PM
/// @section   DESCRIPTION
///            IPFinderFrm class declaration
///
///------------------------------------------------------------------

#ifndef __IPFINDERFRM_H__
#define __IPFINDERFRM_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

////Header Include Start
#include <wx/menu.h>
#include <wx/treectrl.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include "StandardTextCtrl.h"
////Header Include End

////Dialog Style Start
#undef IPFinderFrm_STYLE
#define IPFinderFrm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End


class IPFinderFrm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();

		void WxButton2Click(wxCommandEvent& event);
	public:
		IPFinderFrm(wxString defaultConfigPath, wxString defaultExportPath, wxString portableAppsPath, wxWindow *parent = NULL, wxWindowID id = 1, const wxString &title = wxT("IPFinder"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = IPFinderFrm_STYLE);
		virtual ~IPFinderFrm();
		void MnuwebsiteClick(wxCommandEvent& event);
		void MnuupdateClick(wxCommandEvent& event);
		void MnuexitClick(wxCommandEvent& event);
		void MnuaboutClick(wxCommandEvent& event);
		void MnuSaveClick(wxCommandEvent& event);
		void WxButtonLookupClick(wxCommandEvent& event);
		void DomainLookup(wxString domainName);
		void OnLookupThread(wxCommandEvent& event);
		void WxButtonClearClick(wxCommandEvent& event);
		void WxButtonSaveLogClick(wxCommandEvent& event);
		void WxButtonClearLogClick(wxCommandEvent& event);
		void WxButtonCopyClipboardClick(wxCommandEvent& event);
		void WxTreeCtrlOnContextMenu(wxTreeEvent& event);
		void WxsaveClick(wxCommandEvent& event);
		void MnuexpandallClick(wxCommandEvent& event);
		void MnuexpandClick(wxCommandEvent& event);
		void MnucollapseallClick(wxCommandEvent& event);
		void MnucollapseClick(wxCommandEvent& event);
		void MnulaunchClick(wxCommandEvent& event);
		void MnucopyClick(wxCommandEvent& event);
		void MnuremoveClick(wxCommandEvent& event);
		void HandleButtonLookupState(wxCommandEvent& event);
		void OnCheckUpdateThread(wxCommandEvent& event);
		void OnContextMenu(wxContextMenuEvent& event);
//		void KeyboardBinding(wxKeyEvent& event);
		void CheckForUpdates(bool bCheckSilently);
		wxString GetFormattedResults();
	private:
		wxBoxSizer *WxBoxSizer1;
		wxBoxSizer *WxBoxSizer2;
		wxBoxSizer *WxBoxSizer3;
		wxBoxSizer *WxBoxSizer4;
		wxBoxSizer *WxBoxSizer5;
		wxPanel *WxPanel1;
		wxPanel *WxPanel2;
		wxPanel *WxPanel3;
		wxPanel *WxPanel4;
		wxPanel *WxPanel5;
		wxMenuBar *WxMenuBar1;
		wxStaticText *WxStaticText1;
		wxButton *WxButtonCopyClipboard;
		wxButton *WxButtonClearLog;
		wxButton *WxButtonSaveLog;
		wxButton *WxButtonLookup;
		wxTreeCtrl *WxTreeCtrl1;
//		wxMenu *WxPopupMenu1;
		wxMenu *WxTextCtrlPopupMenu;
		wxMenu *WxTreeCtrlPopupMenu;
		wxMenu *WxOpenWithSubMenu;
		StandardTextCtrl *WxEdit1;

	private:
		enum
		{
			////GUI Enum Control ID Start
			ID_WXBUTTONCOPYCLIPBOARD = 1070,
			ID_WXBUTTONCLEARLOG = 1068,
			ID_WXBUTTONSAVELOG = 1067,
			ID_WXPANEL5 = 1065,
			ID_WXTREECTRL1 = 1091,
			ID_WXPANEL4 = 1057,
			ID_WXBUTTONLOOKUP = 1094,
			ID_WXEDIT1 = 1093,
			ID_WXPANEL3 = 1096,
			ID_WXSTATICTEXT1 = 1089,
			ID_WXPANEL2 = 1035,
			ID_WXPANEL1 = 1098,

			ID_MNU_WEBSITE = 1004,
			ID_MNU_UPDATE = 1006,

			ID_MNU_EXPAND_ALL = 1100,
			ID_MNU_COLLAPSEALL = 1101,
			ID_MNU_REMOVE = 1102,
			ID_MNU_EXPAND = 1103,
			ID_MNU_COLLAPSE = 1104,
			ID_MNU_LAUNCH = 1105,
			ID_MNU_COPY = 1106,
			ID_MNU_LAUNCH_WITH = 1107,

			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

	private:
		void OnClose(wxCloseEvent& event);
		void OpenInBrowser(wxCommandEvent& event);
		void CreateGUIControls();
		void ResetOutput();
		void OutputMessage(wxString message);
		void CopyToClipboard(wxString text);
		void SaveLogToFile();

		bool bIsLookupRunning;
		bool bIsCheckingUpdates;
		bool isPortableApps;

		wxTreeItemId rootItem;
		wxTreeItemId currentChild;

		wxString configPath;
		wxString exportPath;
		wxString pAppsPath;

		wxArrayString systemBrowserPaths;
};


#endif
