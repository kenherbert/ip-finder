///-----------------------------------------------------------------
///
/// @file      StandardTextCtrl.h
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:07 PM
/// @section   DESCRIPTION
///            StandardTextCtrl class declaration
///
///------------------------------------------------------------------

#ifndef STANDARDTEXTCTRL_H
#define STANDARDTEXTCTRL_H

#include <wx/textctrl.h>

class StandardTextCtrl : public wxTextCtrl
{
public:
      StandardTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value = "",
                 const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
                 long style = 0, const wxValidator& validator = wxDefaultValidator,
                 const wxString& name = wxTextCtrlNameStr)
    : wxTextCtrl(parent, id, value, pos, size, style, validator, name) {}
    public:
    void OnContextMenu(wxContextMenuEvent& event);
private:
    DECLARE_EVENT_TABLE()
};
#endif
