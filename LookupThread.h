///-----------------------------------------------------------------
///
/// @file      LookupThread.h
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:07 PM
/// @section   DESCRIPTION
///            LookupThread class declaration
///
///------------------------------------------------------------------

#ifndef LOOKUPTHREAD_H
#define LOOKUPTHREAD_H

#include <wx/thread.h>
#include <wx/event.h>


BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_LOOKUPTHREAD, -1)
END_DECLARE_EVENT_TYPES()

class LookupThread : public wxThread
{
    public:
        LookupThread(wxEvtHandler* pParent, wxString domainName);
        void* CancelLookup();
    private:
        wxString m_domainName;
        bool haltThread;
        void* Entry();
    protected:
        wxEvtHandler* m_pParent;
};
#endif
